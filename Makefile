CFLAGS = -Wall -Wextra `pkg-config --cflags luajit`
LUALDFLAGS = -shared -fPIC -Wl,-rpath='$$ORIGIN' `pkg-config --libs luajit`

CXXFLAGS = -Wall -Wextra
MYTHESLDFLAGS = -shared -fpic `pkg-config --libs mythes`

.PHONY: all clean

all: build/mytheslua.so

build/mytheslua.so: src/mytheslua.c build/mythesc.so | build
	$(CC) src/mytheslua.c $(LUALDFLAGS) -Lbuild -l:mythesc.so $(CFLAGS) -o $@

build/mythesc.so: src/mythesc.cpp | build
	$(CXX) $^ $(CXXFLAGS) $(MYTHESLDFLAGS) -o $@

build:
	mkdir -p $@

clean: build/mytheslua.so build/mythesc.so
	$(RM) $^
