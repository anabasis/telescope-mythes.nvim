local has_telescope, telescope = pcall(require, "telescope")
if not has_telescope then
	error("this plugin requires nvim-telescope/telescope.nvim")
end

local mytheslib         = require "mytheslib"

local actions           = require "telescope.actions"
local action_state      = require "telescope.actions.state"
local conf              = require("telescope.config").values
local entry_display     = require "telescope.pickers.entry_display"
local finders           = require "telescope.finders"
local pickers           = require "telescope.pickers"
local themes            = require "telescope.themes"
local utils             = require "telescope.utils"

local mythes_opts       = {}
local default_thesaurus = nil
local mythes_picker     = function(opts)
	local displayer = entry_display.create({
		separator = " ",
		items = {
			{ width = 40 },
			{ remaining = true },
		},
	})
	local make_display = function(entry)
		return displayer({
			entry.name,
			entry.category,
		})
	end

	local cursor_word = vim.fn.expand "<cword>"
	local suggestions = mytheslib.get_words(cursor_word, default_thesaurus)

	local popts = vim.tbl_deep_extend("force", mythes_opts, opts or {})
	pickers.new(popts, {
		prompt_title = "Thesaurus",
		finder = finders.new_table {
			results = suggestions,
			entry_maker = function(entry)
				return {
					ordinal = entry.category .. entry.name,
					display = make_display,

					name = entry.name,
					value = entry.name:gsub(" %(.+%)", ""),
					category = entry.category,
				}
			end
		},
		sorter = conf.generic_sorter(opts),
		attach_mappings = function(prompt_bufnr)
			actions.select_default:replace(function()
				local selection = action_state.get_selected_entry()
				if selection == nil then
					utils.__warn_no_selection "telescope-mythes"
					return
				end

				action_state.get_current_picker(prompt_bufnr)._original_mode = "i"
				actions.close(prompt_bufnr)
				vim.cmd("normal! ciw" .. selection.value)
				vim.cmd "stopinsert"
			end)
			return true
		end,
	}):find()
end

return telescope.register_extension {
	setup = function(ext_config)
		if ext_config.theme ~= nil and type(ext_config.theme) == "string" then
			mythes_opts = themes["get_" .. ext_config.theme](ext_config)
		else
			mythes_opts = ext_config
		end

		if ext_config.default_thesaurus ~= nil and type(ext_config.default_thesaurus) == "table" and
			(ext_config.default_thesaurus.idx_path ~= nil and ext_config.default_thesaurus.dat_path ~= nil) then
			default_thesaurus = ext_config.default_thesaurus
		end
	end,
	exports = {
		mythes = mythes_picker,
	},
}
