local library_path = string.sub(debug.getinfo(1).source, 2, #"/lua/mytheslib.lua" * -1) .. "build/mytheslua.so"

local mytheslib, err = package.loadlib(library_path, "luaopen_mytheslua")
if not mytheslib then
	vim.api.nvim_err_writeln("Error loading function: " .. err)

	return { get_words = function (_, _) return {} end, }
else
    return mytheslib()
end
