#include <cstdio>
#include <cstring>

#include <mythes.hxx>

extern "C" {
	MyThes* mythes_new(const char* idx_path, const char* dat_path) {
		return new MyThes(idx_path, dat_path);
	}

	void mythes_delete(MyThes* thes) {
		delete thes;
	}

	int mythes_lookup(MyThes* thes, const char* search_str, mentry** entries) {
		return thes->Lookup(search_str, strlen(search_str), entries);
	}

	void mythes_cleanup(MyThes* thes, mentry* entries, int n) {
		thes->CleanUpAfterLookup(&entries, n);
	}
}
