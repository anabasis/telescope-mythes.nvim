#include <luajit-2.1/lua.h>

#include "mythesc.h"

static int get_words(lua_State* L) {
	int argn = lua_gettop(L);
	if (argn < 1) {
		lua_pushliteral(L, "provide at least one argument");
		lua_error(L);

		return 0;
	}

	if (!lua_isstring(L, 1)) {
		lua_pushliteral(L, "provide word to lookup as a string");
		lua_error(L);

		return 0;
	}

	MyThes* thes = NULL;

	if (lua_istable(L, 2)) {
		lua_getfield(L, 2, "idx_path");
		const char* idx_path = lua_tostring(L, -1);

		lua_getfield(L, 2, "dat_path");
		const char* dat_path = lua_tostring(L, -1);

		thes = mythes_new(idx_path, dat_path);
	} else if (lua_isnoneornil(L, 2)) {
		thes = mythes_new("/usr/share/mythes/th_en_US_v2.idx",
						  "/usr/share/mythes/th_en_US_v2.dat");
	} else {
		lua_pushliteral(L, "invalid argument for path");
		lua_error(L);

		return 0;
	}

	const char* word = lua_tostring(L, 1);
	mentry* entries = NULL;

	int n = mythes_lookup(thes, word, &entries);

	// table to store all definitions
	lua_newtable(L);

	int count = 0;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < entries[i].count; j++) {
			// index value
			lua_pushnumber(L, ++count);

			// table to store a definition
			lua_createtable(L, 0, 2);

			lua_pushstring(L, "category");
			lua_pushstring(L, entries[i].defn);
			lua_settable(L, -3);

			lua_pushstring(L, "name");
			lua_pushstring(L, entries[i].psyns[j]);
			lua_settable(L, -3);

			// push definition to all definitions table
			lua_settable(L, -3);
		}
	}

	mythes_cleanup(thes, entries, n);
	mythes_delete(thes);

	return 1;
}

int luaopen_mytheslua(lua_State* L) {
	lua_createtable(L, 0, 1);

	lua_pushstring(L, "get_words");
	lua_pushcfunction(L, &get_words);

	lua_settable(L, -3);

	return 1;
}
