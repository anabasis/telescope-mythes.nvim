typedef struct MyThes MyThes;

typedef struct mentry {
	char* defn;
	int count;
	char** psyns;
} mentry;

MyThes* mythes_new(const char* idx_path, const char* dat_path);

void mythes_delete(MyThes* thes);

int mythes_lookup(MyThes* thes, const char* search_str, mentry** entries);

void mythes_cleanup(MyThes* thes, mentry* entries, int n);
