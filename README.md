# `telescope-mythes.nvim`
Access a thesaurus with [`telescope.nvim`](https://github.com/nvim-telescope/telescope.nvim).

You will need to install [libmythes](https://github.com/hunspell/mythes) in order to use this.

## Configuration
Install `telescope.nvim` and `telescope-mythes.nvim`, and make sure to register it with `telescope.nvim`:
```lua
require("telescope").load_extension("mythes")
```

`telescope-mythes.nvim` needs a build step for the plugin to work; simply run `make` (or add this to the plugin setup in your package manager's configuration).

`telescope-mythes.nvim` exposes setting the theme for the picker and default thesaurus for libmythes to use; see the example below for more information.

### Example Config
```lua
-- example configuration with lazy.nvim
{
    "nvim-telescope/telescope.nvim",

    opts = {
        extensions = {
            mythes = {
                -- choose from "ivy", "dropdown", "cursor"
                theme = "cursor",

                -- defaults to using /usr/share/mythes/th_en_US_v2.{dat,idx}
                default_thesaurus = {
                    dat_path = "/usr/share/mythes/th_es_v2.dat",
                    idx_path = "/usr/share/mythes/th_es_v2.idx",
                },
            },
        },
    },

    -- key bindings to open telescope-mythes
    keys = {
        {
            "<leader>st",
            "<cmd>Telescope mythes<cr>",
            desc = "open Telescope mythes",
        },
    },

    -- necessary dependencies (including telescope-mythes!)
    dependencies = {
        "nvim-lua/plenary.nvim",

		{
            "https://codeberg.org/anabasis/telescope-mythes.nvim",
            build = "make", -- needed!
        },
    },

    -- default config to setup plugin
    config = function(_, opts)
        local telescope = require("telescope")
        telescope.setup(opts)

        telescope.load_extension("mythes")
    end
}
```

## License
`telescope-mythes.nvim` is licensed under the MIT License. See [`./LICENSE`](./LICENSE) for more information.
